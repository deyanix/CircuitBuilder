package eu.deyanix.circuitbuilder;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Circuit {

    public static Circuit loadCircuit(File f) throws ParserConfigurationException, SAXException, IOException {
		/*DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.parse(f);*/
        Document doc = PositionalXMLReader.readXML(new FileInputStream(f));

        Element circuitElement = doc.getDocumentElement();
        Circuit circuit = null;

        if (circuitElement.hasAttribute("not-compile")) {
            try {
                if (Boolean.parseBoolean(circuitElement.getAttribute("not-compile"))) {
                    System.out.println("\tArgument \"not-compile\" equal \"true\". If you generate this module, you must this attribute remove.");
                    return null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        String
                name = Circuit.getAttributeValid(circuitElement, "name", String.class),
                category = Circuit.getAttributeValid(circuitElement, "category", String.class);
        Integer pinInPage = Circuit.getAttributeValid(circuitElement, "pin-in-page", Integer.class);
        if (name != null && category != null && pinInPage != null) {
            circuit = new Circuit(name, category, pinInPage);
        }


        Map<String, Color> colors = new HashMap<>();
        NodeList groups = doc.getElementsByTagName("group");
        for (int i = 0; i < groups.getLength(); i++) {
            Element group = (Element) groups.item(i);
            String
                    id = Circuit.getAttributeValid(group, "id", String.class),
                    color = Circuit.getAttributeValid(group, "color", String.class);
            if (id != null && color != null && circuit != null) {
                colors.put(id, Color.decode(color));
            }
        }

        NodeList pins = doc.getElementsByTagName("pin");
        for (int i = 0; i < pins.getLength(); i++) {
            Element groupElement = (Element) pins.item(i);
            Integer
                    pin = Circuit.getAttributeValid(groupElement, "number", Integer.class); // Integer.parseInt(((Element) pins.item(i)).getAttribute("number"));
            String
                    group = Circuit.getAttributeValid(groupElement, "group", String.class),
                    description = Circuit.getAttributeValid(groupElement, "description", String.class);//((Element) pins.item(i)).getAttribute("description");
            if (pin != null && group != null && description != null && circuit != null) {
                circuit.getDescriptionPin(pin).setColor(colors.get(group));
                circuit.getDescriptionPin(pin).setName(description);
            }
        }

        return circuit;
    }

    private static <T> T getAttributeValid(Element element, String attribute, Class<T> type) {
        if (!element.hasAttribute(attribute)) {
            System.out.println("\tAttribute \"" + attribute + "\" isn't exists in line " + element.getUserData(PositionalXMLReader.LINE_NUMBER_KEY_NAME));
            return null;
        }
        String value = element.getAttribute(attribute);
        T valueConvert;
        try {
            valueConvert = type.cast(value);
        } catch (Exception e) {
            System.out.println("\tAttribute \"" + attribute + "\" isn't " + type.getSimpleName() + " in line " + element.getUserData(PositionalXMLReader.LINE_NUMBER_KEY_NAME));
            return null;
        }

        return valueConvert;
    }


    public class Pin {
        private Color color = Color.black;
        private String name;

        public Color getColor() {
            return color;
        }

        public void setColor(Color color) {
            this.color = color;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    private String name, category; //Nazwa ukladu scalonego
    private Pin[] descriptionPins; //Opisy pinow
    private int pinInPage; //Liczba pinow na strone

    private Circuit() {
    }

    public Circuit(String name, String category, int pinInPage) {
        this.name = name;
        this.category = category;
        descriptionPins = new Pin[pinInPage * 2];
        this.pinInPage = pinInPage;
    }

    public String getName() {
        return name;
    }

    public void setDescriptionPins(String... descPins) {
        if (descPins.length != descriptionPins.length) {
            return;
        }
        for (int i = 0; i < descriptionPins.length; i++) {
            if (descriptionPins[i] == null) {
                descriptionPins[i] = new Pin();
            }
            descriptionPins[i].setName(descPins[i]);
        }
    }

    public String getCategory() {
        return category;
    }

    public Pin[] getDescriptionPins() {
        return descriptionPins;
    }

    public Pin getDescriptionPin(int pin) {
        if (descriptionPins[pin - 1] == null) {
            descriptionPins[pin - 1] = new Pin();
        }
        return descriptionPins[pin - 1];
    }

    public int getPinInPage() {
        return pinInPage;
    }
}
