package eu.deyanix.circuitbuilder;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Arc2D;
import java.awt.image.BufferedImage;

public class CircuitBuilder {
    private Font
            fontDescription, fontPin, fontTitle;
    private int
            pinWidth, pinHeight, pinHeightSpace, imageWidth, imageHeight;

    private Color
            backgroundColor, foregroundColor;

    public Font getFontDescription() {
        return fontDescription;
    }

    public void setFontDescription(Font fontDescription) {
        this.fontDescription = fontDescription;
    }

    public Font getFontPin() {
        return fontPin;
    }

    public void setFontPin(Font fontPin) {
        this.fontPin = fontPin;
    }

    public Font getFontTitle() {
        return fontTitle;
    }

    public void setFontTitle(Font fontTitle) {
        this.fontTitle = fontTitle;
    }

    public int getPinWidth() {
        return pinWidth;
    }

    public void setPinWidth(int pinWidth) {
        this.pinWidth = pinWidth;
    }

    public int getPinHeight() {
        return pinHeight;
    }

    public void setPinHeight(int pinHeight) {
        this.pinHeight = pinHeight;
    }

    public int getPinHeightSpace() {
        return pinHeightSpace;
    }

    public void setPinHeightSpace(int pinHeightSpace) {
        this.pinHeightSpace = pinHeightSpace;
    }

    public int getImageWidth() {
        return imageWidth;
    }

    public void setImageWidth(int imageWidth) {
        this.imageWidth = imageWidth;
    }

    public int getImageHeight() {
        return imageHeight;
    }

    public void setImageHeight(int imageHeight) {
        this.imageHeight = imageHeight;
    }

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public Color getForegroundColor() {
        return foregroundColor;
    }

    public void setForegroundColor(Color foregroundColor) {
        this.foregroundColor = foregroundColor;
    }

    public BufferedImage buildCircuit(Circuit circuit) {
        //Obliczanie wymiarow
        int
                height = ((pinHeight + (2 * pinHeightSpace)) * circuit.getPinInPage()) + (pinHeightSpace * 4),//
                width = (8 * height) / 20,
                imageWidth = width + (150 * 2),
                imageHeight = height + (50 * 2);


        if (imageWidth < this.imageWidth) {
            imageWidth = this.imageWidth;
        }

        if (imageHeight < this.imageHeight) {
            imageHeight = this.imageHeight;
        }

        int
                centerWidth = center(imageWidth, width),
                centerHeight = center(imageHeight, height),
                circle = width / 3;


        //Inicjalizacja ustawien graficznych
        BufferedImage img = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = (Graphics2D) img.getGraphics();
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        FontMetrics
                fmPin = g.getFontMetrics(fontPin),
                fmTitle = g.getFontMetrics(fontTitle),
                fmDescription = g.getFontMetrics(fontDescription);

        g.setColor(backgroundColor);
        g.setFont(fontTitle);
        //Rysowanie ukladu
        g.fillRect(centerWidth, centerHeight, width, height);

        //Wypisywanie kategorii
        Rectangle categoryMetrics = fontMetrics(g, circuit.getCategory(), fontTitle);
        g.drawString(circuit.getCategory(), center(imageWidth, categoryMetrics.width), center(-categoryMetrics.height, 50));

        //Wypisanie tytulu
        g.setColor(foregroundColor);
        Rectangle titleMetrics = fontMetrics(g, circuit.getName(), fontTitle);
        int
                titleX = centerWidth + width - titleMetrics.height - getPinHeightSpace(),//center(imageWidth, titleMetrics.width),
                titleY = centerHeight + getPinHeightSpace();//center(-titleMetrics.height, 50);

        AffineTransform transformOld = g.getTransform();
        g.rotate(Math.toRadians(90), titleX, titleY);
        g.drawString(circuit.getName(), titleX, titleY);
        g.setTransform(transformOld);

        g.setFont(fontPin);
        //Rysowanie wyciecia
        g.fill(new Arc2D.Double(centerWidth + (width / 2) - (circle / 2), centerHeight - (circle / 2), circle, circle, 0, -180, Arc2D.OPEN));

        for (int i = 0; i < circuit.getPinInPage(); i++) {
            int
                    x = centerWidth - pinWidth,
                    y = ((pinHeight + (2 * pinHeightSpace)) * i) + centerHeight + (3 * pinHeightSpace);
            int
                    pinLeft = i + 1,
                    pinRight = (circuit.getPinInPage()) + (circuit.getPinInPage() - i);
            Circuit.Pin
                    pinObjLeft = circuit.getDescriptionPin(pinLeft),
                    pinObjRight = circuit.getDescriptionPin(pinRight);
            String
                    valueLeft = String.valueOf(pinLeft),
                    valueRight = String.valueOf(pinRight),
                    descLeft = pinObjLeft.getName(),
                    descRight = pinObjRight.getName();
            Rectangle
                    pinLeftMetrics = fontMetrics(g, valueLeft, fontPin),
                    pinRightMetrics = fontMetrics(g, valueRight, fontPin),
                    descLeftMetrics = fontMetrics(g, descLeft, fontDescription),
                    descRightMetrics = fontMetrics(g, descRight, fontDescription);

            //Rysowanie pinu lewego
            g.setColor(foregroundColor);
            g.fillRect(x, y, pinWidth, pinHeight);

            //Wypisanie numeru pinu lewego
            g.setColor(backgroundColor);
            g.setFont(fontPin);
            g.drawString(valueLeft, x + center(pinLeftMetrics.width, pinWidth), y + center(-pinLeftMetrics.height, pinHeight));
            g.setColor(pinObjLeft.getColor());
            g.setFont(fontDescription);
            g.drawString(descLeft, center(descLeftMetrics.width, 150 - pinWidth), y + center(-descLeftMetrics.height, pinHeight));

            x = centerWidth + width;
            //Rysowanie pinu prawego
            g.setColor(foregroundColor);
            g.fillRect(x, y, pinWidth, pinHeight);

            //Wypisanie numeru pinu prawego
            g.setColor(backgroundColor);
            g.setFont(fontPin);
            g.drawString(valueRight, x + center(pinRightMetrics.width, pinWidth), y + center(-pinRightMetrics.height, pinHeight));
            g.setColor(pinObjRight.getColor());
            g.setFont(fontDescription);
            g.drawString(descRight, x + pinWidth + center(descRightMetrics.width, 150 - pinWidth), y + center(-descRightMetrics.height, pinHeight));
        }
        g.dispose();

        return img;
    }

    private Rectangle fontMetrics(Graphics2D g, String text, Font font) {
        return font.createGlyphVector(g.getFontRenderContext(), text).getPixelBounds(g.getFontRenderContext(), 0, 0);
    }

    private Rectangle fontMetrics(Graphics2D g, String text) {
        return fontMetrics(g, text, g.getFont());
    }

    private int center(int parent_width, int width) {
        return Math.abs(parent_width - width) / 2;
    }
}
