package eu.deyanix.circuitbuilder;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;

public class Main {

    private static final Integer
            PIN_WIDTH = 35,
            PIN_HEIGHT = 45,
            PIN_HEIGHT_SPACE = 10;

    private static final Color
            FIRST_COLOR = Color.decode("#2e3436"),
            SECOND_COLOR = Color.decode("#babdb6");

    private static final Font
            FONT_SMALL, FONT, FONT_BIG;

    static {
        String fontName = "ArcoPerpetuo-Regular";
        FONT = new Font(fontName, Font.PLAIN, 29);
        FONT_BIG = new Font(fontName, Font.PLAIN, 39);
        FONT_SMALL = new Font(fontName, Font.PLAIN, 18);
    }

    public static void main(String[] args) throws Exception {
        File
                base = new File("base"),
                output = new File("output");
        base.mkdirs();
        output.mkdirs();

        CircuitBuilder builder = new CircuitBuilder();
        builder.setBackgroundColor(Main.FIRST_COLOR);
        builder.setForegroundColor(Main.SECOND_COLOR);

        builder.setFontDescription(Main.FONT_SMALL);
        builder.setFontTitle(Main.FONT_BIG);
        builder.setFontPin(Main.FONT);

        builder.setPinHeight(Main.PIN_HEIGHT);
        builder.setPinHeightSpace(Main.PIN_HEIGHT_SPACE);
        builder.setPinWidth(Main.PIN_WIDTH);

        for (File f : base.listFiles()) {
            System.out.println("Create circuit from file: " + f.getName());
            Circuit circuit = Circuit.loadCircuit(f);
            if (circuit != null) {
                System.out.println("\tLoaded circuit: " + circuit.getName());
                ImageIO.write(builder.buildCircuit(circuit), "PNG", new File(output, f.getName() + ".png"));
                System.out.println("\tGenerate image: Successful");
            } else {
                System.out.println("\tGenerate image: Failed");
            }

        }
    }
}
